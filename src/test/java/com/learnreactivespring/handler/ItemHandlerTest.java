package com.learnreactivespring.handler;


import com.learnreactivespring.ItemReactiveRepository;
import com.learnreactivespring.constants.ItemConstants;
import com.learnreactivespring.document.Item;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@RunWith(SpringRunner.class)
@DirtiesContext
@AutoConfigureWebTestClient
@ActiveProfiles("test")
public class ItemHandlerTest {
    @Autowired
    WebTestClient webTestClient;

    @Autowired
    ItemReactiveRepository itemReactiveRepository;

    public List<Item> data() {
        return Arrays.asList(
                new Item(null, "Samsung TV", 399.00),
                new Item(null, "LG TV", 329.99),
                new Item(null, "Apple Watch", 349.99),
                new Item("ABC", "Beats Headphones", 19.99)
        );
    }

    @BeforeEach
    public void setUp() {
        itemReactiveRepository
                .deleteAll()
                .thenMany(Flux.fromIterable(data()))
                .flatMap(itemReactiveRepository::save)
                .doOnNext(item -> {
                    System.out.println("Inserted item is: " + item);
                }).blockLast();
    }

    @Test
    public void getAllItems() {
        webTestClient
                .get()
                .uri(ItemConstants.ITEM_FUNCTIONAL_END_POINT_V1)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBodyList(Item.class)
                .hasSize(4);
    }

    @Test
    public void getAllItems_approach2() {
        webTestClient
                .get()
                .uri(ItemConstants.ITEM_FUNCTIONAL_END_POINT_V1)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBodyList(Item.class)
                .hasSize(4)
                .consumeWith(response -> {
                    List<Item> items = response.getResponseBody();
                    items.forEach(item -> assertTrue(item.getId() != null));
                });
    }

    @Test
    public void getAllItems_approach3() {
        Flux<Item> itemsFlux = webTestClient
                .get()
                .uri(ItemConstants.ITEM_FUNCTIONAL_END_POINT_V1)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .returnResult(Item.class)
                .getResponseBody();

        StepVerifier.create(itemsFlux.log("Value from network : "))
                .expectNextCount(4)
                .verifyComplete();
    }

    @Test
    public void getOneItem() {
        webTestClient
                .get()
                .uri(ItemConstants.ITEM_FUNCTIONAL_END_POINT_V1 + "/{id}", "ABC")
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.price", "19.99");
    }

    @Test
    public void getOneItem_notFound() {
        webTestClient
                .get()
                .uri(ItemConstants.ITEM_FUNCTIONAL_END_POINT_V1 + "/{id}", "DEF")
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    public void createItem() {
        Item newItem = new Item("DEF", "Iphone x", 999.99);

        webTestClient
                .post()
                .uri(ItemConstants.ITEM_FUNCTIONAL_END_POINT_V1)
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(newItem), Item.class)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.id").isNotEmpty()
                .jsonPath("$.description").isEqualTo("Iphone x")
                .jsonPath("$.price").isEqualTo("999.99");
    }

    @Test
    void updateItem() {
        webTestClient.put()
                .uri(ItemConstants.ITEM_FUNCTIONAL_END_POINT_V1 + "/{id}", "ABC")
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(new Item(null, "updated description", 123.45)), Item.class)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(Item.class)
                .value(item -> {
                    assertThat(item.getId()).isEqualTo("ABC");
                    assertThat(item.getDescription()).isEqualTo("updated description");
                    assertThat(item.getPrice()).isEqualTo(123.45);
                });
    }

  /*  @Test
    void updateItem_NotFound() {
        webTestClient.put()
                .uri(ItemConstants.ITEM_FUNCTIONAL_END_POINT_V1 + "/{id}", "XXX")
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(new Item(null, "updated description", 123.45)), Item.class)
                .exchange()
                .expectStatus().isNotFound();
    }*/

    @Test
    public void deleteItem() {
        webTestClient
                .delete()
                .uri(ItemConstants.ITEM_FUNCTIONAL_END_POINT_V1 + "/{id}", "ABC")
                .exchange()
                .expectStatus().isOk()
                .expectBody(Void.class);
    }

    @Test
    public void runTimeException() {
        webTestClient
                .get()
                .uri("/fun/runtimeException")
                .exchange()
                .expectStatus().is5xxServerError()
                .expectBody()
                .jsonPath("$.message", "Runtime Exception occurred");
    }

}
