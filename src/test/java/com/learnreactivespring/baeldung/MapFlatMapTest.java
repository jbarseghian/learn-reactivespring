package com.learnreactivespring.baeldung;

import org.junit.jupiter.api.Test;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;

// https://www.baeldung.com/java-reactor-map-flatmap
public class MapFlatMapTest {
    //The given mapper converts each item in the input stream to a new item in the output, preserving the order.
    @Test
    public void testMap() {
        Function<String, String> mapper = String::toUpperCase;

        //This mapper converts a string to its uppercase version. We can apply it on a Flux stream:
        Flux<String> inFlux = Flux.just("baeldung", ".", "com");
        Flux<String> outFlux = inFlux.map(mapper);

        StepVerifier.create(outFlux)
                .expectNext("BAELDUNG", ".", "COM")
                .expectComplete()
                .verify();
        //Notice the mapper function isn't executed when the map method is called. Instead, it runs at the time we subscribe to the stream.
    }

    @Test
    public void testFlatMap() {
        // Similar to map, the flatMap operator has a single parameter of type Function.
        // However, unlike the function that works with map,
        // the flatMap mapper function transforms an input item into a Publisher rather than an ordinary object.
        Function<String, Publisher<String>> mapper = s -> Flux.just(s.toUpperCase().split(""));

        // In this case, the mapper function converts a string to its uppercase version,
        // then splits it up into separate characters. Finally, the function builds a new stream from those characters.
        Flux<String> inFlux = Flux.just("baeldung", ".", "com");
        Flux<String> outFlux = inFlux.flatMap(mapper);

        // The flat-mapping operation we've seen creates three new streams out of an upstream with three string items.
        // After that, elements from these three streams are split and intertwined to form another new stream.
        // This final stream contains characters from all three input strings.

        //We can then subscribe to this newly formed stream to trigger the pipeline and verify the output:
        List<String> output = new ArrayList<>();
        outFlux.subscribe(output::add);
        assertThat(output).containsExactlyInAnyOrder("B", "A", "E", "L", "D", "U", "N", "G", ".", "C", "O", "M");
    }
}
