package com.learnreactivespring.handler;

import com.learnreactivespring.ItemReactiveCappedRepository;
import com.learnreactivespring.ItemReactiveRepository;
import com.learnreactivespring.document.Item;
import com.learnreactivespring.document.ItemCapped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import static org.springframework.web.reactive.function.BodyInserters.fromObject;

@Component
public class ItemHandler {
    public static final Mono<ServerResponse> notFound = ServerResponse.notFound().build();

    @Autowired
    ItemReactiveRepository itemReactiveRepository;

    @Autowired
    ItemReactiveCappedRepository itemReactiveCappedRepository;

    public Mono<ServerResponse> getAllItems(ServerRequest serverRequest) {
        return ServerResponse.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(itemReactiveRepository.findAll(), Item.class);
    }

    public Mono<ServerResponse> getOneItem(ServerRequest serverRequest) {
        String id = serverRequest.pathVariable("id");
        Mono<Item> itemFound = itemReactiveRepository.findById(id);

        return itemFound.flatMap(item -> ServerResponse.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(fromObject(item)))
                .switchIfEmpty(notFound);
    }

    public Mono<ServerResponse> createItem(ServerRequest serverRequest) {
        Mono<Item> requestItem = serverRequest.bodyToMono(Item.class);

        return requestItem.flatMap(item ->
                ServerResponse.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(itemReactiveRepository.save(item), Item.class));
    }

    public Mono<ServerResponse> deleteItem(ServerRequest serverRequest) {
        String id = serverRequest.pathVariable("id");
        Mono<Void> deletedItem = itemReactiveRepository.deleteById(id);

        return deletedItem.flatMap(item -> ServerResponse.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(deletedItem, Void.class));
    }

    public Mono<ServerResponse> updateItem(ServerRequest serverRequest) {
        Mono<Item> requestItem = serverRequest.bodyToMono(Item.class);
        String id = serverRequest.pathVariable("id");

        Mono<Item> updatedItem = requestItem.flatMap(item -> {
            Mono<Item> returnItem = itemReactiveRepository.findById(id)
                    .flatMap(currentItem -> {
                        currentItem.setPrice(item.getPrice());
                        currentItem.setDescription(item.getDescription());
                        return itemReactiveRepository.save(currentItem);
                    });
            return returnItem;
        });

        return updatedItem.flatMap(item -> ServerResponse.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(fromObject(item))
                .switchIfEmpty(notFound));
    }

    public Mono<ServerResponse> itemsException(ServerRequest serverRequest) {
        throw new RuntimeException("Runtime Exception occurred");
    }

    public Mono<ServerResponse> itemsStream(ServerRequest serverRequest) {
        return ServerResponse.ok()
                .contentType(MediaType.APPLICATION_STREAM_JSON)
                .body(itemReactiveCappedRepository.findItemsBy(), ItemCapped.class);
    }
}
