package com.learnreactivespring.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@RestController
public class FluxAndMonoController {

    //Browser es un subscriber de flux
    @GetMapping("/flux")
    public Flux<Integer> returnFlux() {
        return Flux.just(1, 2, 3, 4) //no demora 4 segundos en el browser
                .delayElements(Duration.ofSeconds(1))
                .log();
        //browser espera tener todos y retorna el json
    }

    @GetMapping(value = "/fluxstream", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<Integer> returnFluxStream() {
        return Flux.just(1, 2, 3, 4) //demora 4 segundos en el browser
                .delayElements(Duration.ofSeconds(1))
                .log();
        //browser va mostrando por segundo
    }

    @GetMapping(value = "/fluxstream2", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<Long> returnFluxStream2() {
        return Flux.interval(Duration.ofSeconds(1))
                .log();
        //browser va mostrando por segundo
    }

    @GetMapping("/mono")
    public Mono<Integer> returnMono() {
        return Mono.just(1)
                .log();
    }
}
